{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Analyse Flight Route Distances\n",
    "\n",
    "This notebook is designed to process \"Flights_\" .csv files from the [Eurocontrol R&D data archive](https://www.eurocontrol.int/dashboard/rnd-data-archive).\n",
    "\n",
    "This notebook calculates the great circle distance between departure and destination airports and compares it with the \"Actual Distance Flown\".\n",
    "\n",
    "Note: there are a number of caveats about comparing these distances:\n",
    "\n",
    "* the great circle distance between departure and destination airports does not include take-off and landing manouvering distances.\n",
    "* the shortest route may not be the most efficient route; it may be more efficient to fly longer routes that take advantage (or avoid) winds aloft.\n",
    "* the \"Actual Distance Flown\" may calculate distance differently to the great circle function used here.\n",
    "\n",
    "## License\n",
    "\n",
    "This notebook is distributed under the [MIT License](https://bitbucket.org/viaaero/euro-flight-tools/src/master/LICENSE.txt)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import polars as pl\n",
    "from scipy import stats"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def haversine_distance(lat_a, lon_a, lat_b, lon_b):\n",
    "    \"\"\"\n",
    "    Calculate great circle distances between points given their latitudes and longitudes.\n",
    "    It uses the haversine great circle distance equation and numpy ufuncs for fast computation.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    lat_a, lat_b: float\n",
    "        Latitudes of the points, in degrees.\n",
    "        \n",
    "    lon_a, lon_b: float\n",
    "        Longitudes of the points, in degrees.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    The great circle distance between the points, in radians.\n",
    "    \n",
    "    \"\"\"\n",
    "    delta_lat = lat_b - lat_a\n",
    "    delta_lon = lon_b - lon_a\n",
    "    \n",
    "    # \"normalise\" delta_lon so: -180.0 <= delta_lon <= 180.0\n",
    "    delta_lon = np.where(180.0 < delta_lon, delta_lon - 360.0, delta_lon)\n",
    "    delta_lon = np.where(delta_lon < -180.0, delta_lon + 360.0, delta_lon)\n",
    "\n",
    "    # convert angles to radians\n",
    "    lat_a, lat_b, delta_lat, delta_lon = map(np.deg2rad, [lat_a, lat_b, delta_lat, delta_lon])\n",
    "    \n",
    "    # calculate the half-versed-sine (haversine) of the latitude and longitude differences\n",
    "    haversine_dlat = np.square(np.sin(delta_lat / 2))\n",
    "    haversine_dlon = np.square(np.sin(delta_lon / 2))\n",
    "    \n",
    "    a = np.clip(haversine_dlat + np.cos(lat_a) * np.cos(lat_b) * haversine_dlon, 0.0, 1.0)\n",
    "    \n",
    "    # the great circle distance in radians\n",
    "    return 2.0 * np.arcsin(np.sqrt(a))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gc_distance_nm(lat_a, lon_a, lat_b, lon_b):\n",
    "    \"\"\"\n",
    "    Calculate great circle distances between points given their latitudes and longitudes.\n",
    "    It simply calls haversine_distance and converts the distance to Nautical Miles using\n",
    "    the definition of a Nautical Mile as one minute of arc of latitude.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    lat_a, lat_b: float\n",
    "        Latitudes of the points, in degrees.\n",
    "        \n",
    "    lon_a, lon_b: float\n",
    "        Longitudes of the points, in degrees.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    The great circle distance between the points, in Nautical Miles.\n",
    "    \n",
    "    \"\"\"\n",
    "    return 60.0 * np.rad2deg(haversine_distance(lat_a, lon_a, lat_b, lon_b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Flights file fields\n",
    "ID_FIELD = 'ECTRL ID'\n",
    "FOBT_FIELD = 'FILED OFF BLOCK TIME'\n",
    "AOBT_FIELD = 'ACTUAL OFF BLOCK TIME'\n",
    "DEP_LAT = 'ADEP Latitude'\n",
    "DEP_LON = 'ADEP Longitude'\n",
    "DES_LAT = 'ADES Latitude'\n",
    "DES_LON = 'ADES Longitude'\n",
    "ACT_DIST = 'Actual Distance Flown (nm)'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the Eurocontrol \"Flights\" csv file\n",
    "filename = input('Enter filename: ')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "flights_df = pl.scan_csv(filename).select(\n",
    "    [ID_FIELD, DEP_LAT, DEP_LON, DES_LAT, DES_LON, ACT_DIST]\n",
    ").drop_nulls().collect()\n",
    "flights_df.schema"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "flights_df.drop_nans()\n",
    "size = flights_df.height\n",
    "dep_des_distance = np.empty(size)\n",
    "delta_distance = np.empty(size)\n",
    "size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# Calculate the great circle distance between departure and destination airports\n",
    "dep_des_distance = gc_distance_nm(flights_df[DEP_LAT], flights_df[DEP_LON], flights_df[DES_LAT], flights_df[DES_LON])\n",
    "stats.describe(dep_des_distance)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# Don't consider flights that returned to their destination airports\n",
    "delta_distance = np.where(dep_des_distance > 0.0, flights_df[ACT_DIST] - dep_des_distance, 1.0)\n",
    "stats.describe(dep_des_distance)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dev",
   "language": "python",
   "name": "dev"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
