#!/usr/bin/env python
#
# Copyright (c) 2020-2025 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys
import errno
import polars as pl
from datetime import datetime, timedelta
from timeit import default_timer as timer

"""
This file extracts the rows of a Flights .csv file where the FILED OFF BLOCK TIME
was on the given date and within the given time period.

Note: neither the filename dates nor the csv field dates conform to the
ISO 8601 standard date time format. The given date must be in the filename format.

The input file must be in csv format and may be compressed in bz2 or gzip format.
"""

ID_FIELD = 'ECTRL ID'
FOBT_FIELD = 'FILED OFF BLOCK TIME'

DATE_FORMAT = '%d-%m-%Y'
""" The format of a date string in a csv field. """

TIME_FORMAT = '%H:%M'
""" The format of a time string in a csv field. """

DATE_TIME_FORMAT = ' '.join([DATE_FORMAT, TIME_FORMAT])
""" The format of a date time string in a csv field. """

FILENAME_DATE_FORMAT = '%Y%m%d'
""" The format of a date string in a filename. """

if len(sys.argv) < 3:
    print('Usage: extract_flights_over_period.py <filename> <date> <start_hour> <duration>')
    sys.exit(errno.EINVAL)

# get the input parameters
filename = sys.argv[1]
date = sys.argv[2]

start_hour = '0'
if 3 < len(sys.argv):
    start_hour = sys.argv[3]

print('start_hour:', start_hour)

duration = '1'
if 4 < len(sys.argv):
    duration = sys.argv[4]

print('duration:', duration)

# convert the date into python datetime format and calculate the start and end times
py_date = datetime.strptime(date, FILENAME_DATE_FORMAT)
py_hour = timedelta(hours=int(start_hour))
py_start_time = py_date + py_hour

py_delta = timedelta(hours=int(duration))
py_end_time = py_start_time + py_delta

# convert the date and next day into strings matching the csv date time format.
start_time = py_start_time.strftime(DATE_TIME_FORMAT)
end_time = py_end_time.strftime(DATE_TIME_FORMAT)

# create the output file name from the date
output_filename = '_'.join(['Flights', date, start_hour, duration])
output_filename += '.csv'

print('reading flights from:', filename)
time0 = timer()

# Open the csv file and read the rows
flights_lf = pl.scan_csv(filename)

time1 = timer()

# print('pl.scan_csv time (secs)', (time1 - time0))

# Select flights within the range: start_time to end_time
date_lf = flights_lf.filter(
    (pl.col(FOBT_FIELD) > start_time) &
    (pl.col(FOBT_FIELD) <= end_time)
)

time2 = timer()

# print('select flights time (secs)', (time2 - time1))

date_lf.collect().write_csv(output_filename)

print('written flights to:', output_filename)
