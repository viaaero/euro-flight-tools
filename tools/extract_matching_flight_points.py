#!/usr/bin/env python
#
# Copyright (c) 2020-2025 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys
import errno
import polars as pl
from timeit import default_timer as timer

"""
This file extracts the rows of a Flight_Points .csv file that have matching
ids with the flights in a Flights .csv file and writes them into a new
Flight_Points file named from the Flights file.

The input files must be in csv format and may be compressed in bz2 or gzip format.
"""

ID_FIELD = 'ECTRL ID'

if len(sys.argv) < 3:
    print('Usage: extract_matching_flight_points.py <flights_filename> <flight_points_filename>')
    sys.exit(errno.EINVAL)

# get the filename and date from the input parameters
flights_filename = sys.argv[1]
flight_points_filename = sys.argv[2]

flights_fields = flights_filename.split('_')
flights_points_fields = flight_points_filename.split('_')

# create the output file name from the flights_points_fields and the flights_fields
output_points_fields = flights_points_fields[0: 3]
del flights_fields[0]
output_points_fields.extend(flights_fields)
output_filename = '_'.join(output_points_fields)

print(f'Reading {flights_filename} and {flight_points_filename}')
time0 = timer()

# Just read the ID_FIELD column from the flights file
flights_lf = pl.scan_csv(flights_filename).select([ID_FIELD])
points_lf  = pl.scan_csv(flight_points_filename)

# Join the flight points with the flight ids and write it
merge_lf = points_lf.join(flights_lf, on=ID_FIELD, how="left", coalesce=True)
merge_lf.collect().write_csv(output_filename)

time1 = timer()
# print('select flight points time (secs)', (time1 - time0))

print(f'Written flight points to: {output_filename}')
