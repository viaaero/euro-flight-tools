# euro-flight-tools

A collection of python tools to help analyse aviation data in the
Eurocontrol R&D data archive.  
The archive is open for all R&D use.

To access the archive data you must first register with Eurocontrol see: [Eurocontrol R&D data archive](https://www.eurocontrol.int/dashboard/rnd-data-archive).

## Description

The Eurocontrol R&D data archive contains many files, most of which contain a months flight data and are quite large, currently up to 0.5 GB (compressed).

### tools

The large size of the data files makes them slow to load and analyse, even when
reading them using [LazyFrame](https://docs.pola.rs/api/python/stable/reference/lazyframe/index.html).  
It is often easier to use smaller files initially when developing analysis
scripts before running them on a whole months data.

The tools directory contains the following python scripts to extract smaller
data sets in the same format as the original data files:

* extract_daily_flights.py - extracts flights with a FILED OFF BLOCK TIME on a given date.
* extract_flights_over_period.py - extracts flights with a FILED OFF BLOCK TIME 
within a given period on a given date.
* extract_matching_flight_points.py - extracts filed or actual flight points
for the flights in a flights file.

### notebooks

Much of the flight data contains geographic positions, it is useful to
visualise the positions.

The notebooks directory contains the following jupyter notebooks to visualise
flight routes:

* analyse_flight_route_distances.ipynb - calculate and compare the distance
between departure and destination airports and the "Actual Distance Flown" from
"Flights_" files .
* visualise_route.ipynb - displays filed or actual flight points on a `folium` map.
* visualise_filed_and_actual_routes.ipynb - simultaneously displays both
filed and actual flight points on a `folium` map.

## Dependencies

The `tools` use the [polars](https://docs.pola.rs/api/python/stable/reference/index.html) python library.  
The Jupyter `notebooks` use [polars](https://docs.pola.rs/api/python/stable/reference/index.html)
and the [folium](https://pypi.org/project/folium/) python libraries.

## License

`euro-flight-tools` is provided under a MIT license, see [LICENSE](LICENSE.txt).

Contact <enquiries@via-technology.aero> for more information.
